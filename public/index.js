
import {
    getAccountInfo,
    getAppsList,
    createApp,
    deleteApp,
    testProxy
} from './appApis.js'


//import jquery
// import $ from './jquery.js';


// var toke = 'fffc32bff69f709800de738b6acb4c926c39d128a403765d018f03e795b30fdf';
var toke = '';
var isloaded = false;
var appslist = [];
var branch = 'main'; //main
var proxy;

(async()=>{
    var korim = await testProxy(proxy);
    console.log(korim);
})();

$('#tokenInput').on('keyup', async function () {
    toke = $(this).val();
    showAccountInfo();
});
$('#proxyDetails').on('keyup', async function () {
    proxy = $(this).val();
    showAccountInfo();
});


//show info about the app
showAccountInfo();


//create app
//-----------------------------------------------------
$('#createApp').on('click', async function (e) {
    e.preventDefault();

    if (!appslist?.apps?.length) {
        console.log('no apps');
        var appName = $('#appName').val();
        var repo_branch = branch; //$('#repo_branch').val(); //master
        var repo_path = $('#gitRepo').val();

        if (!toke) {
            alert('no account selected enter token');
            return;
        }

        if (!appName) {
            alert('Please enter app name');
            return;
        }

        if (!repo_path) {
            alert('Please enter git repo path');
            return;
        }
    } else {
        console.log('have apps');
        var services = appslist.apps[appslist.apps.length - 1].active_deployment.spec.services[0];
        var appName = services.name;
        if (services.github) {
            var repo_branch = services.github.branch;
            var repo_path = `https://github.com/${services.github.repo}`;
        } else {
            var repo_branch = services.gitlab.branch;
            var repo_path = `https://gitlab.com/${services.gitlab.repo}`;
        }

        console.log(services);
    }

    //app name list
    var appNameList = Array(10).fill().map((_, i) => {
        return appName + '-' + Math.floor(Math.random() * 100)
    });



    //createApps
    await Promise.all(appNameList.map(async (naame) => {
        console.log({
            appName: naame,
            repo_branch,
            repo_path
        });
        await createApps({
            token: toke,
            appName: naame,
            repo_branch,
            repo_path
        });
    }));


    //load app links
    appslist = await getAppsList(toke);
    loadAppLinks();
});

//delete all apps
//-----------------------------------------------------
$('#deleteButton').on('click', async function (e) {
    e.preventDefault();
    if (!appslist?.apps?.length) {
        $('#deletedInfo').html(`<span style="color:red">haven't any app on this account</span>`);
        return false;
    } else {
        // get first 10 from appslist
        // var some = appslist.apps.slice(0, 8);
        var some = appslist.apps
        await Promise.all(some.map(async (app) => {
            var app = await deleteApp(toke, app.id);
            $(`#deletedInfo`).html(`Deleted-App-ID: ${app.id}`);
        }));
        $('#deletedInfo').html(`<span style="color:green">all app has been deleted</span>`);
        //load app links
        appslist = await getAppsList(toke);
        loadAppLinks();
    }
});

//show account info
//-----------------------------------------------------
async function showAccountInfo() {
    //proxy details
    $('#proxyInfo').html(`<img src="img/loading.gif" />`);
    var ipInfo = await testProxy(proxy);
    if (ipInfo.ip) {
        $('#proxyInfo').html(`
            <strong>IP: </strong>
            <span>${ipInfo.ip}</span>
            <br>
            <strong>CITY: </strong>
            <span>${ipInfo.city}</span>

            <a href="#" id="showFullProxy">full details</a>
        `);
    } else {
        $('#proxyInfo').html(`<span style="color:red">${ipInfo.message}</span>`);
    }

    //account info
    if (!toke) return $('#email').html(`<span>no account selected</span>`);
    $('#email').html(`<img src="img/loading.gif" />`);
    isloaded = false;
    var accountinfo = await getAccountInfo(toke);
    if (accountinfo.message) {
        $('#email').html(`<span style="color:red">${accountinfo.message}</span>`);
        return false;
    } else {
        $('#email').html(`
        <strong>ACCOUNT: </strong><br>
        <span style="color:green">${accountinfo.account.email}</span>
        `);
    }

    //get user apps list
    appslist = await getAppsList(toke);
    console.log(appslist);
    isloaded = true;
    //load app links
    loadAppLinks();
}

//create 10 apps
//-----------------------------------------------------
async function createApps({
    token,  
    appName,
    repo_branch,
    repo_path
}) {
    return new Promise(async (resolve, reject) => {

        $('#cratingInfo').html(`
            <span>creating: ${appName}<span>
            <img src="img/loading.gif" />
        `);

        var createRequst = await createApp({
            token: token,
            name: appName,
            repo_branch,
            repo_path
        });

        //load app links
        appslist = await getAppsList(toke);
        loadAppLinks();

        if (createRequst.message) {
            $('#cratingInfo').html(`
                <span style="color:red">${createRequst.message}</span>
            `);
        } else {
            $('#cratingInfo').html(`
                <span style="color:green">created: ${createRequst.app.spec.name}</span>
            `);
        }
        resolve(createRequst);
    });
}



//load app links
//-----------------------------------------------------
function loadAppLinks() {
    // get apps links
    // and show them in the table
    if (!appslist?.apps?.length) {
        $('#gitrepoInputs').show();
        $('#appsLinks').html(`<span style="color:green">haven't any app on this account</span>`);
        return false;
    } else {
        $('#gitrepoInputs').hide();
        var appslinks = appslist.apps.map(app => {
            return {
                url: app.live_url,
                appname: app.spec.name,
            };
        });
        $('#appsLinks').html(appslinks.map((link) => {
            return link.url ? `<a href="${link.url}" target="_blank">${link.appname}</a>` : `(${link.appname}: deploing: <img src="img/loading.gif" />)`;
        }).join('&nbsp;'));
    }

    //get only links
    var urls = appslinks.map(link => {
        return link.url;
    });

    //on click on copy button
    $('#copyBtn').on('click', function (e) {
        e.preventDefault();
        copyToClipboard(urls.join('\n'));
    });
}

//reloadBtn click
//load app links
//-----------------------------------------------------
$('#reloadBtn').on('click', async function (e) {
    e.preventDefault();
    $('#cratingInfo').html("");
    $('#deletedInfo').html("");
    $('#appsLinks').html(`<img src="img/loading.gif" />`);
    appslist = await getAppsList(toke);
    loadAppLinks();
});

//display proxy info
//-----------------------------------------------------
$(document).on('click', '#showFullProxy', async function () {
    var ipInfo = await testProxy(proxy);
    alert(`
        city: ${ipInfo.city}
        country: ${ipInfo.country}
        ip: ${ipInfo.ip}
        loc: ${ipInfo.loc}
        org: ${ipInfo.org}
        postal: ${ipInfo.postal}
        readme: ${ipInfo.readme}
        region:  ${ipInfo.region}
        timezone: ${ipInfo.timezone}
    `);
});


//copy the text to clipboard
//-----------------------------------------------------
function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}