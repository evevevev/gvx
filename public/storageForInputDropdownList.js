var tokenList = [];
var proxyList = [];
var gitRepoList = [];

//get data form local storage
getDataFromLocalStorage();

function getDataFromLocalStorage() {
    tokenList = JSON.parse(localStorage.getItem('tokenList')) ? JSON.parse(localStorage.getItem('tokenList')) : [];
    proxyList = JSON.parse(localStorage.getItem('proxyList')) ? JSON.parse(localStorage.getItem('proxyList')) : [];
    gitRepoList = JSON.parse(localStorage.getItem('gitRepoList')) ? JSON.parse(localStorage.getItem('gitRepoList')) : [];
}

$('#tokenInput').on('change', async function () {
    var token = $(this).val();
    if (token == '' || tokenList.includes(token)) return false;
    tokenList.unshift(token);
    //save to local storage
    localStorage.setItem('tokenList', JSON.stringify(tokenList));
});
$('#proxyDetails').on('change', async function () {
    var proxy = $(this).val();
    if (proxy == '') return false;
    proxyList.unshift(proxy);
    //save to local storage
    localStorage.setItem('proxyList', JSON.stringify(proxyList));
});
$('#gitRepo').on('change', function () {
    var gitRepo = $(this).val();
    if (gitRepo == '') return false;
    gitRepoList.unshift(gitRepo);
    //save to local storage
    localStorage.setItem('gitRepoList', JSON.stringify(gitRepoList));
});



//auto complete search
function autocomplete(inp, arr, arrstr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("focus", async function (e) {
        // alert('asdf')
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        // if (!val) {
        //     return false;
        // }

        // const request = await fetch(`https://suggest.trovi.com/SuggestionFeed/Suggestion?format=json&q=${val}`);
        // const response = await request.json();
        // arr = response[1];

        // remove element 
        if (document.getElementById(this.id + "autocomplete-list")) {
            document.getElementById(this.id + "autocomplete-list").remove();
        }

        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            // if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            b.innerHTML = arr[i];
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            b.innerHTML += "<button class='deleteSavedItem'>delete</buttton>";
            b.addEventListener("click", function (e) {
                // if not delete button
                if (!e.target.classList.contains('deleteSavedItem')) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                } else if (e.target.classList.contains('deleteSavedItem')) {
                    // if delete button
                    var value = this.getElementsByTagName("input")[0].value;
                    var index = arr.indexOf(value);
                    arr.splice(index, 1);
                    this.remove();

                    //save to local storage
                    localStorage.setItem(arrstr, JSON.stringify(arr));
                }
            });

            a.appendChild(b);
            // }
        }
    }, true);
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {

                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
                else
                    doSearch();
            } else {
                doSearch();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        if (!e.target.matches('.autocomplete-items, .autocomplete-items *, .input-section *, .deleteSavedItem')) {
            closeAllLists(e.target);
        }
    });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda",
    "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain",
    "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
    "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria",
    "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
    "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands",
    "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark",
    "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea",
    "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France",
    "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar",
    "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana",
    "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland",
    "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya",
    "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia",
    "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi",
    "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico",
    "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique",
    "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia",
    "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau",
    "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal",
    "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa",
    "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles",
    "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa",
    "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent",
    "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania",
    "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan",
    "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
    "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam",
    "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"
];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("tokenInput"), tokenList, "tokenList");
autocomplete(document.getElementById("proxyDetails"), proxyList, "proxyList");
autocomplete(document.getElementById("gitRepo"), gitRepoList, "gitRepoList");