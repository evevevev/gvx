//test Proxy with fetch and httpsProxyAgent
//-----------------------------------------------------
export async function testProxy(proxyDetails) {
    const response = await fetch('/testProxy', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            proxyDetails: proxyDetails
        })
    });
    const json = await response.json();
    return json;
    // // var proxyDetails = "IP:Port:Username:password:protocol";  // protocol will be HTTPS OR SOCKS5 OR SOCKS4 OR HTTP
    // // var proxyDetails = "38.102.79.4:1490:abcd:abcd:HTTPS"; 
    // // var proxyDetails = "38.74.10.104:1490:abcd:abcd:HTTPS"; 
    // // var proxyDetails = "45.43.86.32:443:ayxibire:st703pon65sp:socks5"; 
    // if (proxyDetails) {
    //     try {
    //         var proxy = proxyDetails.split(":");
    //         // proxyAgent  = new HttpsProxyAgent.HttpsProxyAgent(`socks5://${proxy[2]}:${proxy[3]}@${proxy[0]}:${proxy[1]}`);
    //         proxyAgent = new HttpsProxyAgent({
    //             host: proxy[0],
    //             port: proxy[1],
    //             auth: proxy[2] + ":" + proxy[3],
    //             protocol: proxy[4]+":",
    //             rejectUnauthorized: true,
    //         });
    //         // proxyAgent = new HttpsProxyAgent({
    //         //     host: proxy[0],
    //         //     port: proxy[1],
    //         //     auth: proxy[2] + ":" + proxy[3],
    //         //     rejectUnauthorized: false,
    //         // });
    //         const response = await fetch('https://ipinfo.io/json', {
    //             agent: proxyAgent
    //         });
    //         const json = await response.json();
    //         console.log(json);
    //         if(json.ip == proxy[0]){
    //             console.log("Proxy is working");
    //             // $('#checkIcon').show()
    //         }
    //         return json;
    //     } catch (error) {
    //         console.log(error);
    //         proxyAgent = null;
    //         // $('#checkIcon').hide()
    //         return {
    //             "status": "error",
    //             "message": "Please check proxy details"
    //         }
    //     }
    // } else {
    //     const response = await fetch('https://ipinfo.io/json', {
    //         agent: proxyAgent
    //     });
    //     const json = await response.json();
    //     return json;
    // }
}

//get account info
export async function getAccountInfo(token) {
    const response = await fetch('/getAccountInfo', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            token: token
        })
    });
    const json = await response.json();
    return json;
}

//get apps list 
export async function getAppsList(token) {
    const response = await fetch('/getAppsList', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            token: token
        })
    });
    const json = await response.json();
    return json;
}


// create app
export async function createApp({
    token,
    name,
    repo_branch,
    repo_path,
}) {

    const response = await fetch('/createApp', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            token: token,
            name: name,
            repo_branch: repo_branch,
            repo_path: repo_path
        })
    });
    const json = await response.json();
    return json;
}

//delete app
export async function deleteApp(token, id) {
    const response = await fetch('/deleteApp', {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            token: token,
            id: id
        })
    });
    const json = await response.json();
    return json;
}


// add profile
//------------------------------------------------------
export async function addProfile(profile) {
    const response = await fetch('/addProfile', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            profile: profile
        })
    });
    const json = await response.json();
    return json;
}
// get profiles
export async function getProfiles(ID) {
    const response = await fetch('/getProfiles', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: ID? ID : null
        })
    });
    const json = await response.json();
    return json;
}   
// delete profile   
export async function deleteProfile(ID) {
    const response = await fetch('/deleteProfile', {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: ID,
        })
    });
    const json = await response.json();
    console.log(json);
    return json;
} 