import {
    addProfile,
    getProfiles,
    deleteProfile,
} from './appApis.js';


var profileList = [];

getProfiles().then(function (profileList) {
    profileList = profileList;
    showProfiles(profileList);
});

$('.profile-section form').on('submit', async function (e) {
    e.preventDefault();
    //get from data
    var formData = $(this).serializeArray();
    var formDataObj = {};
    formData.forEach(function (item) {
        formDataObj[item.name] = item.value;
    });
    formDataObj["id"] = new Date().getTime(); //add id    

    //add to db 
    profileList = await addProfile(formDataObj);
    showProfiles(profileList);

    //reset form
    $(this)[0].reset(); 
});

function showProfiles(profileList) {
    $('#profiles').empty();
    profileList.forEach(function (profile) {
        $('#profiles').append(`
            <div class="profile-item" data-id="${profile.id}">
                <div>
                    <h3>${profile.name}</h3>
                    <p>${profile.tokenInput}</p>
                    <p>${profile.gitRepo}</p>
                    <p>${profile.proxy}</p>
                </div>
                <button class="btn btn-danger item-deletebtn" data-id="${profile.id}">Delete</button>
            </div>
        `);
    });
}

//delete profile
$(document).on('click', '.item-deletebtn', async function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    profileList = await deleteProfile(id);
    showProfiles(profileList);
});

// on select profile
$(document).on('click', '.profile-item', async function (e) {
    e.preventDefault();
    // show loading
    $('.selectedProfile').html('<img src="img/loading.gif" />');
    var id = $(this).attr('data-id');
    profileList = await getProfiles();
    var profile = profileList.find(function (profile) {
        return profile.id == id;
    });
    $('.selectedProfile').html(profile.name);
    setProfile(profile);
});

// set profile
function setProfile(profile) {
    $('#appName').val(profile.name);
    $('#tokenInput').val(profile.tokenInput);
    $('#gitRepo').val(profile.gitRepo);
    $('#proxyDetails').val(profile.proxy);

    // press key to trigger change event
    $('#tokenInput').trigger('keyup');
    $('#gitRepo').trigger('keyup'); 
    $('#proxyDetails').trigger('keyup');
}


// dropdown menu
$('.dropbtn').on('click', function myFunction() {
    document.getElementById("profiles").classList.toggle("show");
})

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn, .item-deletebtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}