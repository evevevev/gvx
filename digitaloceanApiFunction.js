const fetch = require('node-fetch');
const HttpsProxyAgent = require('https-proxy-agent');

var proxyAgent;

//test Proxy with fetch and httpsProxyAgent
//-----------------------------------------------------
async function testProxy({proxyDetails}) {
    
    // var proxyDetails = "IP:Port:Username:password:protocol";  // protocol will be HTTPS OR SOCKS5 OR SOCKS4 OR HTTP
    // var proxyDetails = "38.102.79.4:1490:abcd:abcd:HTTPS"; 
    // var proxyDetails = "38.74.10.104:1490:abcd:abcd:HTTPS"; 
    // var proxyDetails = "45.43.86.32:443:ayxibire:st703pon65sp:socks5"; 
    if (proxyDetails) {
        try {
            var proxy = proxyDetails.split(":");
            // proxyAgent  = new HttpsProxyAgent.HttpsProxyAgent(`socks5://${proxy[2]}:${proxy[3]}@${proxy[0]}:${proxy[1]}`);
            proxyAgent = new HttpsProxyAgent({
                host: proxy[0],
                port: proxy[1],
                auth: proxy[2] + ":" + proxy[3],
                protocol: proxy[4]+":",
                rejectUnauthorized: true,
            });
            // proxyAgent = new HttpsProxyAgent({
            //     host: proxy[0],
            //     port: proxy[1],
            //     auth: proxy[2] + ":" + proxy[3],
            //     rejectUnauthorized: false,
            // });
            const response = await fetch('https://ipinfo.io/json', {
                agent: proxyAgent
            });
            const json = await response.json();
            console.log(json);
            if(json.ip == proxy[0]){
                console.log("Proxy is working");
                // $('#checkIcon').show()
            }
            return json;
        } catch (error) {
            console.log(error);
            proxyAgent = null;
            // $('#checkIcon').hide()
            return {
                "status": "error",
                "message": "Please check proxy details"
            }
        }
    } else {
        const response = await fetch('https://ipinfo.io/json', {
            agent: proxyAgent
        });
        const json = await response.json();
        return json;
    }
}

//get account info
async function getAccountInfo({token}) {
    const response = await fetch('https://api.digitalocean.com/v2/account', {
        agent: proxyAgent,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
    });
    const json = await response.json();
    return json;
}

//get apps list 
async function getAppsList({token}) {
    const response = await fetch('https://api.digitalocean.com/v2/apps', {
        agent: proxyAgent,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
    });
    const json = await response.json();
    return json;
}


// create app
async function createApp({
    token,
    name,
    repo_branch,
    repo_path,
}) {

    var services;
    var url = new URL(repo_path);
    var path = url.pathname.substring(1);

    if (repo_path.includes('github.com')) {
        services = [{
            "name": name,
            "github": {
                "branch": repo_branch,
                "deploy_on_push": true,
                "repo": path
            },
            // "run_command": "bin/api",
            // "environment_slug": "node-js",
            // "instance_count": 1,
            "instance_size_slug": "basic-xxs",
            "routes": [{
                "path": "/",
            }]
        }]
    } else {
        services = [{
            "name": name,
            "gitlab": {
                "branch": repo_branch,
                "deploy_on_push": true,
                "repo": path
            },
            // "run_command": "bin/api",
            // "environment_slug": "node-js",
            // "instance_count": 1,
            "instance_size_slug": "basic-xxs",
            "routes": [{
                "path": "/",
            }]
        }]
    }

    const response = await fetch('https://api.digitalocean.com/v2/apps', {
        agent: proxyAgent,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
            "spec": {
                "name": name,
                "region": "nyc",
                "services": services
            }
        }),
    });
    const json = await response.json();
    return json;
}

//delete app
async function deleteApp({token, id}) {
    const response = await fetch('https://api.digitalocean.com/v2/apps/' + id, {
        agent: proxyAgent,
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
    });
    const json = await response.json();
    return json;
}


module.exports = {
    getAccountInfo,
    getAppsList,
    createApp,
    deleteApp,
    testProxy
};