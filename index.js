require('dotenv').config()
const fs = require('fs');
const express = require('express');
// const playwright = require('playwright');
// const TelegramBot = require('node-telegram-bot-api');


var profileList = [];

const {
    getAccountInfo,
    getAppsList,
    createApp,
    deleteApp,
    testProxy
} = require("./digitaloceanApiFunction.js");

const app = express()
const port = 3000;

//body parser middleware
app.use(express.json());
app.use(express.static('public'))

//get token from env
// const token = process.env.TELEGRAM_TOKEN;
// var toke = 'dop_v1_2708c9fd24d03a645b29f031f6b93187040f032f144dc5a9734358e4c42dc2dc'

//html routes
app.get('/', (req, res) => {
    //   res.send(list)
    // parce html from public folder
    res.sendFile(__dirname + '/public/index.html');
});

//get account info
// {
//     "token": "digitalocean_token"
// }
app.post('/getAccountInfo', async (req, res) => {
    res.send(await getAccountInfo(req.body));
});

//get apps list
// {
//     "token": "digitalocean_token"
// }
app.post('/getAppsList', async (req, res) => {
    res.send(await getAppsList(req.body));
});

//create app 
// {
//     "token": "digitalocean_token",
//     "name": "app_name",
//     "repo_branch": "main",
//     "repo_path": "git_repo_path"
// }
app.post('/createApp', async (req, res) => {
    res.send(await createApp(req.body));
});

//delete apps from digital ocean
// {
//     "token": "digitalocean_token",
//     "id": "app_id"
// }
app.delete('/deleteApp', async (req, res) => {
    res.send(await deleteApp(req.body));
});

//test Proxy server
// {
//     "proxyDetails": "proxy_details"
// }
app.post('/testProxy', async (req, res) => {
    res.send(await testProxy(req.body));
});


//profile
//------------------------------------------------------
app.post('/addProfile', async (req, res) => {
    profileList.push(req.body.profile);
    res.send(profileList);

    //save to file
    saveProfile();
});
app.post('/getProfiles', async (req, res) => {
    res.send(profileList);

    //save to file
    saveProfile();
});
app.delete('/deleteProfile', async (req, res) => {
    profileList = profileList.filter(profile => profile.id != req.body.id);
    res.send(profileList);

    //save to file
    saveProfile();
});

// get profile from file
var profileList = JSON.parse(fs.readFileSync(__dirname + '/profile.json'));

//save profile to file
function saveProfile() {
    fs.writeFileSync(__dirname + '/profile.json', JSON.stringify(profileList));
}



app.listen(port, () => {
    //get the ip address of the server
    const ifaces = require('os').networkInterfaces();
    const ip = Object.keys(ifaces).reduce((prev, curr) => {
        ifaces[curr].forEach(iface => {
            if (iface.family === 'IPv4' && iface.internal === false) {
                prev = iface.address;
            }
        });
        return prev;
    }, null);
    console.log(`Server is running on ${ip}:${port}`);
})